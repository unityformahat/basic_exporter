import psycopg2  # PostgreSQL api tool for python
import pprint  # This is printing json nicely

class DbMannager:

    def __init__(self):
        self.connection = self.postgressql_connect()


    def postgressql_connect(self):
        """
        This function connecting to postgresSQL DB.

        :return: The connection status (connected / failed)
        """
        try:
            connection = psycopg2.connect(
                user="unity_usr@galileo-dbs ",
                password="unity_usr123",
                host="galileo-dbs.postgres.database.azure.com",
                port="5432",
                database="unity_db"
            )
            print("Connection succesfully to PostgresSQL")
            return connection

        except (Exception, psycopg2.Error) as error:
            print("Error connection to DB", error)

    def postgressql_select_queries(self, query):
        try:
            cursor = self.connection.cursor()
            cursor.execute(query)
            holder = cursor.fetchall()
            cursor.close()
            return holder

        except (Exception, psycopg2.Error) as error:
            print("Error in Query", error)


    def postgressql_execute(self, table_name, list_of_enteties):
        try:
            cursor = self.connection.cursor()

            columns= self.get_all_collums_names(table_name)

            num_of_values_for_query= "%s" + ", %s" *(len(columns)-1)
            sql_query="""INSERT INTO unity_schema."""+table_name+""" VALUES (""" + num_of_values_for_query + """) ON CONFLICT("""+ columns[0]+ """) DO UPDATE SET """+self.get_index_of_collums(columns)
            cursor.executemany(sql_query, list_of_enteties)
            self.connection.commit()


        except (Exception, psycopg2.Error) as error:
            print("Error in Query", error)

        finally:
            cursor.close()


    @staticmethod
    def get_num_of_collums(list_of_enteties):
        return len(list_of_enteties[0])


    def get_index_of_collums(self,columns):
        returnindex=""
        for i in range(1 ,len(columns)-1):
            returnindex+=columns[i]+" = EXCLUDED."+columns[i]+", "
        returnindex += columns[len(columns)-1] +" = EXCLUDED."+columns[len(columns)-1]
        return returnindex


    def get_all_collums_names(self, table_name):
        columns=[]
        cursor = self.connection.cursor()
        Query = """SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '""" +table_name+"""'"""
        cursor.execute(Query)
        col_names = cursor.fetchall()

        for tup in col_names:
            columns += [tup[0]]
        # print("printing all columns")
        # print(columns)
        # print("done")
        return columns


    def close_connection(self):
        self.connection.close()
        print("connection close")


