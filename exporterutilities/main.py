# import pprint  # This is printing json nicely
#
# from atlassian import Jira  # atlassian api tool for python
#
# from exporterutilities.DbMannager import DbMannager
#
#
#
#
# def getJira():
#     """Returns Jira object. (initializing URL, USERNAME, PASSWORD)"""
#     jira = Jira(
#             url="http://52.169.187.138:8080/",
#             username="admin",
#             password="admin")
#
#     return jira
#
#
#
# def GetJiraEpicIssues():
#     """"Returns all jira`s Epics via list object"""
#
#     jira = getJira()
#     # Initializing all visible projects to jiraProjects variable
#
#     jira_projects = jira.projects(included_archived=None)
#     list_of_epic_issues = []
#
#     try:
#         for jira_project in jira_projects:
#
#             project_issues=jira.get_all_project_issues(jira_project["id"], fields='*all')
#             #pprint.pprint(project_issues)
#
#             for project_issue in project_issues:
#                 # pprint.pprint(project_issue)
#                 # checking if there is an assignee so we can get his name
#                 if project_issue["fields"]["assignee"] is not None:
#                     assignee_display_name = project_issue["fields"]["assignee"]["displayName"]  # Assignee
#                 else:
#                     assignee_display_name = None
#
#                 if project_issue["fields"]["issuetype"]["name"] == "Epic":
#                     line_of_epic_issue = (
#
#                         project_issue["key"],                                             #EpicID
#                         project_issue["fields"]["summary"],                               #Name
#                         project_issue["fields"]["created"],                               #CreationTime
#                         project_issue["fields"]["reporter"]["name"],                      #ReporterID
#                         jira_project["id"],                                               #ProjectID
#                         None,                                                             #PorfolioEpicID
#                         project_issue["fields"]["status"]["statusCategory"]["name"],      #Status
#                         assignee_display_name)                                            #Assigne
#                     list_of_epic_issues.append(line_of_epic_issue)
#                     print (line_of_epic_issue)
#
#         return list_of_epic_issues
#     except ValueError as projectError:
#         print("Failed to initialize issues, Error: %", projectError)
#
#
# dbm = DbMannager()
# dbm.postgressql_execute("epics", GetJiraEpicIssues())
#


