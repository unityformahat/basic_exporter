import unittest
from DbMannager import DbMannager

class MyTestCase(unittest.TestCase):
    DbMannager=DbMannager()

    def test_get_num_of_collums(self):
        test_list= [(15, "hello", "guy", "scrum"),(18,"yes","lol","kanban")]
        self.assertEquals(4,DbMannager.get_num_of_collums(test_list))

    DbMannager.close_connection()
if __name__ == '__main__':
    unittest.main()
