import setuptools
from setuptools import find_packages
from distutils.core import setup
import os

build_number = os.environ['BUILD_NUMBER']
version = os.environ['RELEASE_MAJOR'] +"."+ os.environ['RELEASE_MINOR'] +"."+ build_number

setup(
        name='exporterutilities',
        version=version,
        packages=find_packages(),
        url='http://nexus.owls.shlomke.xyz/#browse/browse:unity',
        license='',
        author='team_devops',
        author_email='',
        description='a library that helps to push and pull data from PostgresSQL'
)
